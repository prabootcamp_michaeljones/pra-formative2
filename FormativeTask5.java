
class Test5 {

    public static void main(String[] args) {

        /*
         * Declare and initialisation the variable with an array type from integer
         * there are only length 3 consist 1, 2 and 3
         */
        int arr[] = { 1, 2, 3 };

        /*
         * Make a loop with each array 'arr' and assign it to variable final int i
         */
        for (final int i : arr)
            System.out.print(i + " "); // the Output will print the concatenation array of variable i and " "
    }
}

/*
 * The result will be: 1 2 3
 * When the code executed, it will be success without any error
 * the final local variable of 'i' was the variable that can't be assignment in
 * loop
 * Because if the assignment for example was like : i = i + 1,
 * It will be error because the final integer variable can't be change
 */