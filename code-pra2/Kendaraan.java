public interface Kendaraan {
    static final int RODA = 2;

    public void setMesinPembakaran(boolean mesinPembakaran);

    public boolean getMesinPembakaran();
}
