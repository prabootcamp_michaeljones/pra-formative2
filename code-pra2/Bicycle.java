public class Bicycle implements Kendaraan {
    private boolean mesinPembakaran;

    public void setMesinPembakaran(boolean mesinPembakaran) {
        this.mesinPembakaran = mesinPembakaran;
    }

    public boolean getMesinPembakaran() {
        return mesinPembakaran;
    }
}
