import java.util.Scanner;

public class FormativeTask6 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int input;
        System.out.println("==============================");
        System.out.println("Sepeda atau Motor???");
        System.out.println("1. Bicycle");
        System.out.println("2. Motorcycle");
        System.out.println("==============================");
        System.out.print("Pilih nomor dari atas: ");
        input = keyboard.nextInt();

        Bicycle sepeda = new Bicycle();
        sepeda.setMesinPembakaran(false);

        Motorcycle sepedaMotor = new Motorcycle();
        sepedaMotor.setMesinPembakaran(true);

        // Eksekusi output
        if (input == 1) {

            System.out.println("==============================");

            System.out.println("B I C Y C L E");
            System.out.println("\nRoda Sepeda ada: " + Kendaraan.RODA + " Roda");
            System.out.println("Mesin Sepeda: " + sepeda.getMesinPembakaran());
        } else if (input == 2) {

            System.out.println("==============================");

            System.out.println("M O T O R C Y C L E");
            System.out.println("\nRoda Sepeda Motor ada: " + Kendaraan.RODA + " Roda");
            System.out.println("Mesin Sepeda Motor: " + sepedaMotor.getMesinPembakaran());
        } else
            System.out.println("Harap mengisi sesuai dengan nomor diatas");
    }
}
