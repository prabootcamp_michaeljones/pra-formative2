
public class Test {

    public static void main(String[] args) {
        /*
         * Declare and instantiate the variable (create an object) 'obj' with
         * type non-primitive from class 'Test'
         */
        Test obj = new Test();

        // After declare and instantiate the variable, we use a method from class 'test'
        obj.start(); // Call the 'start' method and will be executed.
    }

    public void start() {

        // Declare a string local variable name 'stra' with a value "do"
        String stra = "do";

        /*
         * declare a string local variable name 'strb' with a return value from 'method'
         * function the method will receive a string from variable 'stra'
         */
        String strb = method(stra);

        /*
         * make a concatenation on an outpot
         * with String "; " + variable 'stra' and 'strb'
         */
        System.out.print(": " + stra + strb);
    }

    /*
     * Declare a new public modifier with a String return type
     * make the 'method' as a name of the method and a String parameter name 'stra'
     */
    public String method(String stra) {

        /*
         * make a assignment on variable 'stra'
         * the amount of 'stra' is the concatenation between parameter 'stra' and a
         * string value "good"
         */
        stra = stra + "good";
        System.out.print(stra);
        return " good"; // return the value with String type and the value "good"
    }
}

/*
 * Result: dogood: do good
 * After start() is called the variable 'stra' valeu was 'do'
 * variable 'strb' get the value from the return of 'method'
 * after the 'strb' function was done, the output print that will appear first
 * is 'stra' and the value will be "dogood" because 'stra = stra+good'
 * the 'strb' will be return the value become "good"
 * and the final output print will be (dogood: do good)
 * because 'stra + strb' become "do good"
 */