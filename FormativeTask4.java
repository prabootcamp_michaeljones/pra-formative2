
public class Test2 {
    // deklarasi integer a dan b dengan value a = 1, b = 2
    int a = 1;
    int b = 2;

    // Kesimpulan 1
    public Test2 func(Test2 obj) {

        Test2 obj3 = new Test2();
        obj3 = obj;
        obj3.a = obj.a++ + ++obj.b;
        obj.b = obj.b;
        return obj3;
    }

    // Kesimpulan 2
    public static void main(String[] args) {
        Test2 obj1 = new Test2();
        Test2 obj2 = obj1.func(obj1);
        System.out.println("obj1.a = " + obj1.a + " obj1.b = " + obj1.b);
        System.out.println("obj2.a = " + obj2.a + " obj1.b = " + obj2.b);
        System.out.println("obj2 = " + obj2);
        System.out.println("obj1 = " + obj1);

    }
}
// Kesimpulan 1

/*
 * Metode 'func' dari return type 'Test2 'akan mengambil tipe Test2 sebagai
 * parameter dan mengembalikan objek yang ada di type Test2
 * Pada metode ini, Test2 akan membuat objek baru yaitu 'obj3' dari type Test2
 * Kemudian menetapkan 'obj' dan melakukan penambahan dasar pada variabel int a
 * dan b dari objek-objek.
 * Dan di akhir perintah, akan melakukan return pada obj3.
 */

// Kesimpulan 2

/*
 * Method 'main' akan menjadi method yang akan mengeksekusi program utama
 * 'obj1' akan direferensikan dari Test2 sebagai objek baru
 * 'obj2' akan direferensikan dari metode func dengan parameter 'obj1'
 * Perintah 'obj1' akan direturn ke 'obj2' dan akan mengeluarkan output nilai
 * dari variable integer a dan b untuk kedua return type yaitu 'obj1' dan 'obj2'
 */